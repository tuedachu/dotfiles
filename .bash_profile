#
# ~/.bash_profile
# t

[[ -f ~/.bashrc ]] && . ~/.bashrc

eval $(ssh-agent)

udiskie &

export GIT_EDITOR="emacsclient"
export EDITOR="emacsclient"

export PATH="$PATH:$HOME/.local/bin"
export GOPATH="$HOME/projects/go"

export PATH="$PATH:$GOPATH/bin"

## pvt-utils variables
export PHAZECOMP_RESET_LICENSE=0
export PHAZECOMP_PATH="/home/tuedachu/whitson/02-DEV/pvt-utils/phz_binaries/PhazeComp176"
export PVT_UTILS_RUNNING_IN_CLOUD=1
export PHZ_TEMPLATE_PATH="/home/tuedachu/whitson/02-DEV/pvt-utils/phz_templates"
export PVT_UTILS_BACKUP_DIR="/home/tuedachu/whitson/02-DEV/pvt-utils/backup"
export PVT_UTILS_TEST_DATA_PATH="/home/tuedachu/whitson/02-DEV/pvt-utils/tests/data"
export PVT_UTILS_DATA_PATH="/home/tuedachu/whitson/02-DEV/pvt-utils/data"
export EXCEL_TEMPLATE_PATH="/home/tuedachu/whitson/02-DEV/pvt-utils/excel_templates"
export GOOGLE_APPLICATION_CREDENTIALS="/home/tuedachu/whitson/02-DEV/pvt-utils/personal/google_credentials.json"
export PVT_UTILS_N_PHZ_PROCESSES=1
export PVT_UTILS_LOG_ERROR_TO_ISSUE_TRACKER=0
export WHITSON_PVT_UTILS_SECRET_KEY="epN1xxj^QJX4#kd!Bl6DtTcqn5\$JdLKY6et!Y8sh2r&P_y*@|?W+0%ItFa+HN@m-nV#uEv1-t-|YSE_RVpJMYhmmf74mEnnzA72m"

export GRPC_DNS_RESOLVER="native"


source ~/wtf-completion.sh

test(){
    echo "hello world"
}
