#!/bin/sh

# git-changelog --  A program to automatically generate changelogs from git

# Copyright (C) 2020 Arnaud Hoffmann

# Authors: A. Hoffmann <tuedachu@gmail.com>
# Version: 1.1.4
# URL: https://gitlab.com/tuedachu/git-change-log

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#  Code:

PROGRAM=$(basename "$0")
VERSION=1.1.4

RED='\033[0;31m\033[1m'
GREEN='\033[0;32m\033[1m'
BOLD='\033[0m\033[1m'
NORMAL='\033[0m'

usage="Usage:

$PROGRAM [-h] [-v] [-f <commit-tag-filter>] [-s <time-duration>] [-f <version-tag>] [-t <version-tag] [-o output] [-F format] [-0 <prefix>] [-1 <postfix>] [-a] [-k]

where:
    -h    show this help text
    -v	  get program version
    -c    set the commit-tag(s) to look for (DEFAULT: ALL)
    -s    set date from which to show commits (see --since option in git)
    -f    only consider commits since this version tag (Default: last version tag)
    -t    only consider commits up to that version tag (Default: HEAD)
    -o 	  set output file to print the change log
    -F    set the format of the output file among 'org', 'md', 'txt' (Default: Emacs org-mode)
    -0    section title prefix when writing a changelog file
    -1    section title postfix when writing a changelog file
    -a    show author in commit line
    -k    list commit keywords (can be configured with env. variable 'GIT_CHANGELOG_KEYWORDS')"


msg() #Print argument in color declared in argument 2
{
    echo -e $2$1
}

print_program()
{
    msg "===  $PROGRAM $VERSION  ===" $BOLD
    echo -e $NORMAL"A program to automatically generate changelogs from git"
    msg "" $NORMAL
}

print_usage()
{
 echo "$usage"
 exit $1
}

if [ ! -z "$GIT_CHANGELOG_KEYWORDS" ]; then
    git_changelog_keywords=$GIT_CHANGELOG_KEYWORDS
else
    git_changelog_keywords="FEATURE SUB-FEATURE PERF BUG HOTFIX DOC INTERNAL TEST STYLE REFACTOR CHORE EXAMPLE"
fi

split_keywords()
{
    IFS=' ' read -ra git_changelog_keywords <<< "$git_changelog_keywords"    #Convert string to array
}

from=$(git describe --tags --abbrev=0 --always)
to=HEAD
commit_tag=ALL
output_file=""
out_format="org"
allowed_out_formats=['org','md','txt']
since=""
commit_format="%s"
section_prefix=DEFAULT
section_postfix=DEFAULT
while getopts ':hvc:s:f:t:o:F:a0:1:k' option; do
  case "$option" in
    h) print_program
       print_usage 0
       exit 0
       ;;
    v) msg "v $VERSION" $NORMAL
       exit 0
       ;;
    c) commit_tag=$OPTARG
       ;;
    s) since=$OPTARG
       ;;
    f) from=$OPTARG
       ;;
    t) to=$OPTARG
       ;;
    o) output_file=$OPTARG
       ;;
    F) out_format=$OPTARG
       if [[ ! ${allowed_out_formats[*]} =~ d ]]; then
	   msg "" $RED
	   echo "ERROR: output format '$out_format' is not allowed!"
	   print_usage 1
       fi
       ;;
    a) commit_format="%s (by %an)"
       ;;
    0) section_prefix=$OPTARG
	 ;;
    1) section_postfix=$OPTARG
       ;;
    k) print_program
       split_keywords
       echo "List of commit keywords: " ${git_changelog_keywords[@]}
	exit 0
       ;;
    :) msg "" $RED
       printf "ERROR: Missing argument for -%s\n" "$OPTARG">&2
       print_usage 1
       ;;
   \?) msg "" $RED
       printf "ERROR: Illegal option: -%s\n" "$OPTARG" >&2
       print_usage 1
       ;;
  esac
done
shift $((OPTIND - 1))

print_program

if [ ! -z $output_file ]; then
    echo "Changelog generated by $PROGRAM $VERSION." > $output_file
    echo "" >> $output_file
    echo "FROM: $from" >> $output_file
    echo "TO: $to" >> $output_file
    echo "" >> $output_file
    if [ $out_format = "org" ]; then
	if [ $section_prefix = "DEFAULT" ]; then
	    section_prefix="*"
	fi
	if [ $section_postfix = "DEFAULT" ]; then
	    section_postfix="*"
	fi
    elif [ $out_format = "md" ]; then
	if [ $section_prefix = "DEFAULT" ]; then
	    section_prefix="**"
	fi
	if [ $section_postfix = "DEFAULT" ]; then
	    section_postfix="**"
	fi
    else
	if [ $section_prefix = "DEFAULT" ]; then
	    section_prefix=""
	fi
	if [ $section_postfix = "DEFAULT" ]; then
	    section_postfix=""
	fi
    fi
fi

launch_git()
{
    if [ -z $since ]; then
	git log $from..$to --pretty=format:"$commit_format" | grep -i -E "^\[$1\]" | awk -v tag="$1" '{gsub(tag,"",$0); gsub(/\[|\]/, "", $0); print $0}'
    else
	git log --since=$since --pretty=format:"$commit_format" | grep -i -E "^\[$1\]"  | awk -v tag="$1" '{gsub(tag,"",$0); gsub(/\[|\]/, "", $0); print $0}'
    fi
}

get_commits()
{
    results=$(launch_git $1)
    if [ ! -z "$results" ]; then
       echo "$1:"
       launch_git $1
       echo ""
       if [ ! -z $output_file ]; then
	   echo "$section_prefix$1$section_postfix:" >> $output_file
	   echo "" >> $output_file
	   launch_git $1 | awk '{print "-" $0}' >> $output_file
	   echo "" >> $output_file
       fi
    fi
}

if [ $commit_tag = "ALL" ]; then
    if [ -z $since ]; then
	msg "Generating change log from $from to $to looking for all tagged commits..." $GREEN
    else
	msg "Generating change log produced in the last $since looking for all tagged commits..." $GREEN
    fi
    msg "" $NORMAL
    for commit_tag in $git_changelog_keywords
    do
	get_commits $commit_tag
    done
else
    if [ -z $since ]; then
	msg "Generating change log from $from to $to looking for commits containing $commit_tag..." $GREEN
    else
	msg "Generating change log produced in the last $since looking for containing $commit_tag..." $GREEN
    fi
    msg "" $NORMAL
    msg "Changelog:" $NORMAL
        launch_git $commit_tag
fi




