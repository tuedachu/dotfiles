(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/"))
(add-to-list 'package-archives '("org" ."http://orgmode.org/elpa/"))
(package-initialize)

(setq package-selected-packages '(2048-game all-the-icons auctex auto-complete-auctex company-go company-jedi company-tern company-web disk-usage doom-modeline elisp-def elpy emacsql-sqlite exwm faceup find-file-in-project fish-completion flycheck-package flycheck forge closql emacsql ghub gif-screencast gnus-alias go-eldoc go-mode graphql helm-company company helm-emms emms helm-mu helm-notmuch helm-pass auth-source-pass helm-pydoc helm-sly helm-system-packages helm-youtube helm helm-core async highlight-indentation htmlize ivy jedi jedi-core epc ctable concurrent js2-mode keycast log4e lsp-mode lua-mode lv macrostep magit git-commit magit-popup magit-section markdown-mode matrix-client frame-purpose ht esxml kv a dash-functional anaphora mu4e-conversation notmuch org-jira org-plus-contrib ov package-lint paredit password-store pdf-tools pinentry pkg-info epl pos-tip prettier-js py-autopep8 pydoc python-environment deferred pyvenv quack racket-mode rainbow-identifiers request scheme-complete shrink-path f s sly smartparens dash spinner tablist tern-auto-complete auto-complete popup tern tracking transient transmission treepy undo-tree queue use-package bind-key vue-mode edit-indirect ssass-mode vue-html-mode mmm-mode web-completion-data web-mode wgrep-helm wgrep which-key windower with-editor compat ws-butler xelb yaml yaml-mode yasnippet))

(package-refresh-contents)
(package-install-selected-packages)
(all-the-icons-install-fonts)
