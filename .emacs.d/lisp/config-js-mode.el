;; Inspired from 'https://emacs.cafe/emacs/javascript/setup/2017/04/23/emacs-setup-javascript.html'

;; (define-key js2-mode-map (kbd "M-.") (lambda()
;;                                        (interactive)
;;                                        (helm-grep-do-git-grep t)))


(require 'company)
(require 'company-tern)
(require 'lsp-mode)

(use-package lsp-mode
  :commands lsp)

(use-package vue-mode
  :mode "\\.vue\\'"
  :config
  (add-hook 'vue-mode-hook #'lsp))


(defun tuedachu-js-mode-setup ()
  (tern-mode t)
  (set (make-local-variable 'company-backends) '(company-tern))
  (company-mode))

(add-hook 'js2-mode-hook 'tuedachu-js-mode-setup)
(add-hook 'js2-mode-hook 'prettier-js-mode)

(autoload 'helm-company "helm-company")
(with-eval-after-load 'company
  (define-key company-mode-map (kbd "<M-tab>") 'helm-company)
  (define-key company-active-map (kbd "<M-tab>") 'helm-company))

(setq company-idle-delay nil)

(defun delete-tern-process ()
  (interactive)
  (delete-process "Tern"))

;; Disable completion keybindings, as we use xref-js2 instead
;; (define-key tern-mode-keymap (kbd "M-.") nil)
;; (define-key tern-mode-keymap (kbd "M-,") nil)


(add-hook 'vue-mode-hook
          (lambda ()
            (set-face-background 'mmm-default-submode-face nil)
            (linum-mode)) )

(add-hook 'vue-mode-hook
          'prettier-js-mode)

(add-hook 'vue-mode-hook
          'tuedachu-js-mode-setup)

(setq prettier-js-args '("--print-width" "120"
                         "--trailing-comma" "es5"
                         "--tab-width" "4"))

;; VUE related stuff
;; - All getters in helm
;; - All components in helm
;; - Insert

(provide 'config-js-mode)
