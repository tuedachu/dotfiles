

(defun whitson--create-new-eshell (buffer-name commands)
  (let ((eshell-buffer-name buffer-name))
    (eshell)
    (dolist (command commands)
      (insert command)
      (message command)
      (eshell-send-input))))

(defun whitson--source-env-var (env-file)
  (let (env-var)
    (find-file env-file)
    (setq env-var (buffer-string))
    (kill-buffer)
    env-var))

(defun whitson:create-fullstack-environment ()
  (interactive)
  (let* ((pvtu-local (yes-or-no-p "Do you want to use pvt-utils locally?"))
         (api-env-var (concat (whitson--source-env-var "~/whitson/02-DEV/plus-api/.env.dev")
                              (whitson--source-env-var "~/whitson/02-DEV/plus-api/.env.local")
                              (when pvtu-local
                                "export GOOGLE_PUBSUB_TOPIC=arnaud-to-calculate"))))

    (shell-command (concat "echo " (shell-quote-argument (read-passwd "[sudo] password:"))
                           " | sudo -S systemctl start postgresql"))
    (whitson--create-new-eshell "frontend" '("cd ~/whitson/02-DEV/plus-frontend/"
                                             "export NODE_OPTIONS=--openssl-legacy-provider"
                                             "npm run dev"))
    (whitson--create-new-eshell "plus-api" `("cd ~/whitson/02-DEV/plus-api/"
                                             ,api-env-var
                                             "flask resetdb"))
    (whitson--create-new-eshell "plus-api-listener" `("cd ~/whitson/02-DEV/plus-api/"
                                                      ,api-env-var
                                                      "flask consume_from_pubsub"))
    (when pvtu-local
      (whitson--create-new-eshell "pvt-utils" '("cd ~/whitson/02-DEV/pvt-utils/"
                                                "python pvtu-local.py")))))

(defun whitson:create-fullstack-environment-no-reset ()
  (interactive)
  (let* ((pvtu-local (yes-or-no-p "Do you want to use pvt-utils locally?"))
         (api-env-var (concat (whitson--source-env-var "~/whitson/02-DEV/plus-api/.env.dev")
                              (whitson--source-env-var "~/whitson/02-DEV/plus-api/.env.local")
                              (when pvtu-local
                                "export GOOGLE_PUBSUB_TOPIC=arnaud-to-calculate"))))

    (shell-command (concat "echo " (shell-quote-argument (read-passwd "[sudo] password:"))
                           " | sudo -S systemctl start postgresql"))
    (whitson--create-new-eshell "frontend" '("cd ~/whitson/02-DEV/plus-frontend/"
                                             "export NODE_OPTIONS=--openssl-legacy-provider"
                                             "npm run dev"))
    (whitson--create-new-eshell "plus-api" `("cd ~/whitson/02-DEV/plus-api/"
                                             ,api-env-var
                                             "flask run"))
    (whitson--create-new-eshell "plus-api-listener" `("cd ~/whitson/02-DEV/plus-api/"
                                                      ,api-env-var
                                                      "flask consume_from_pubsub"))
    (when pvtu-local
      (whitson--create-new-eshell "pvt-utils" '("cd ~/whitson/02-DEV/pvt-utils/"
                                                "python pvtu-local.py")))))

(defun whitson:populate_db ()
  (interactive)
  (switch-to-buffer "plus-api")
  (insert (concat
           "flask populatedb "
           (read-from-minibuffer "Fields to populated: " "Permian Bakken")
           " --demo"))
  (eshell-send-input))

(defun whitson:dbeaver ()
  (interactive)
  (start-process-shell-command "dbeaver" nil "dbeaver"))


(defun rabbit-mq-port-forwarding()
  (interactive)
  (insert "kubectl port-forward svc/plus-rabbitmq -n rabbitmq 5672:5672"))
