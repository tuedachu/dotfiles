;; EMMS
(require 'emms-setup)
(emms-all)
(require 'emms-player-mpv)
(add-to-list 'emms-player-list 'emms-player-mpv)
(require 'emms-info-libtag)
(setq emms-info-functions '(emms-info-libtag))
(setq emms-source-file-default-directory "~/music/")
(add-to-list 'emms-player-mpv-parameters "--no-audio-display")

(setq emms-browser-covers 'emms-browser-cache-thumbnail)

(define-emms-source playlist (file)
  "An EMMS source for a native EMMS playlist file."
  (interactive (list (read-file-name "Playlist file: "
                                     "~/playlists/"
                                     ""
                                     t)))
  (mapc #'emms-playlist-insert-track
        (with-temp-buffer
          (emms-insert-file-contents file)
          (goto-char (point-min))
          (when (not (emms-source-playlist-native-p))
            (error "Not a native EMMS playlist file."))
          (emms-source-playlist-parse-native file))))

(define-key emms-playlist-mode-map (kbd "s-a") 'emms-add-playlist)

(provide 'config-emms)
